package gui;

import dao.BdConexao;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import util.FileManager;
import util.StringUtil;
import util.Zip;

public class Principal extends javax.swing.JFrame {

    public String diretorioPacote;
    public String versaoPacote;
    
    public Principal() {
        initComponents();
        
        this.setSize(600, 320);
        // INSERINDO ITENS DE CONEXÕES TNS
        inserindoItensConnTns(comboBoxConexoesTns);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jDialog1 = new javax.swing.JDialog();
        jDialog2 = new javax.swing.JDialog();
        painelAutenticacaoBanco = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnAplicarPacote = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btnUsuarioBanco = new java.awt.TextField();
        btnSenhaBanco = new java.awt.TextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        label1 = new java.awt.Label();
        labelAutenticacaoBanco = new java.awt.Label();
        labelSelecionePacote = new java.awt.Label();
        labelPacoteAplicado = new java.awt.Label();
        painelSelecionePacote = new javax.swing.JPanel();
        btnProsseguirAutenticacaoBd = new javax.swing.JButton();
        btnUploadFile = new javax.swing.JButton();
        comboBoxConexoesTns = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        painelPacoteAplicado = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton4 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setPreferredSize(new java.awt.Dimension(600, 320));
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        painelAutenticacaoBanco.setToolTipText("");

        jLabel1.setText("Usuario do banco:");

        jLabel3.setText("Senha do banco:");

        btnAplicarPacote.setText("Aplicar pacote");
        btnAplicarPacote.setEnabled(false);
        btnAplicarPacote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAplicarPacoteActionPerformed(evt);
            }
        });

        jButton2.setText("< Voltar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btnUsuarioBanco.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        btnUsuarioBanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsuarioBancoActionPerformed(evt);
            }
        });
        btnUsuarioBanco.addTextListener(new java.awt.event.TextListener() {
            public void textValueChanged(java.awt.event.TextEvent evt) {
                btnUsuarioBancoTextValueChanged(evt);
            }
        });

        btnSenhaBanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSenhaBancoActionPerformed(evt);
            }
        });
        btnSenhaBanco.addTextListener(new java.awt.event.TextListener() {
            public void textValueChanged(java.awt.event.TextEvent evt) {
                btnSenhaBancoTextValueChanged(evt);
            }
        });

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jTextArea2.setText("vai ter um texto aki\n\nenquanto isso ignora.");
        jTextArea2.setEnabled(false);
        jScrollPane2.setViewportView(jTextArea2);

        javax.swing.GroupLayout painelAutenticacaoBancoLayout = new javax.swing.GroupLayout(painelAutenticacaoBanco);
        painelAutenticacaoBanco.setLayout(painelAutenticacaoBancoLayout);
        painelAutenticacaoBancoLayout.setHorizontalGroup(
            painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelAutenticacaoBancoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUsuarioBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSenhaBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(painelAutenticacaoBancoLayout.createSequentialGroup()
                        .addGroup(painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(painelAutenticacaoBancoLayout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                        .addComponent(btnAplicarPacote, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        painelAutenticacaoBancoLayout.setVerticalGroup(
            painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelAutenticacaoBancoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(painelAutenticacaoBancoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUsuarioBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSenhaBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(painelAutenticacaoBancoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(btnAplicarPacote))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimidia/logo.png"))); // NOI18N

        label1.setFont(new java.awt.Font("Verdana", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(0, 0, 102));
        label1.setName(""); // NOI18N
        label1.setText("Aplicador de pacotes");

        labelAutenticacaoBanco.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        labelAutenticacaoBanco.setText("Autenticação do banco > ");

        labelSelecionePacote.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        labelSelecionePacote.setText("Selecione o pacote > ");

        labelPacoteAplicado.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        labelPacoteAplicado.setText("Pacote aplicado!");

        painelSelecionePacote.setToolTipText("");

        btnProsseguirAutenticacaoBd.setText("Prosseguir > ");
        btnProsseguirAutenticacaoBd.setEnabled(false);
        btnProsseguirAutenticacaoBd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProsseguirAutenticacaoBdActionPerformed(evt);
            }
        });

        btnUploadFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimidia/upload.png"))); // NOI18N
        btnUploadFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadFileActionPerformed(evt);
            }
        });

        comboBoxConexoesTns.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                comboBoxConexoesTnsInputMethodTextChanged(evt);
            }
        });
        comboBoxConexoesTns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxConexoesTnsActionPerformed(evt);
            }
        });

        jLabel2.setText("Faça upload do arquivo:");

        jLabel5.setText("Selecione sua conexão Tns:");

        javax.swing.GroupLayout painelSelecionePacoteLayout = new javax.swing.GroupLayout(painelSelecionePacote);
        painelSelecionePacote.setLayout(painelSelecionePacoteLayout);
        painelSelecionePacoteLayout.setHorizontalGroup(
            painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelSelecionePacoteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUploadFile, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProsseguirAutenticacaoBd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(painelSelecionePacoteLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(comboBoxConexoesTns, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelSelecionePacoteLayout.setVerticalGroup(
            painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelSelecionePacoteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelSelecionePacoteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(painelSelecionePacoteLayout.createSequentialGroup()
                        .addComponent(comboBoxConexoesTns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnProsseguirAutenticacaoBd))
                    .addComponent(btnUploadFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 37, Short.MAX_VALUE))
        );

        jButton3.setText("Aplicar outro pacote");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setText("Log da aplicação");
        jTextArea1.setEnabled(false);
        jScrollPane1.setViewportView(jTextArea1);

        jButton4.setText("Aplicar reversão");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("ATENÇAO: JA Existem 2 objetos invalidos. ");

        jLabel8.setText("Procedures com erros:");

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Banco\\INTEGRA\\PI_GERA_PRENOTA_VENDA_ORDEM.sql", "Banco\\INTEGRA\\PI_INTERNALIZA_NFE.SQL", "Banco\\INTEGRA\\PI_INTERNAL_ECOMMERCE_MADEIRA.sql", "Banco\\INTEGRA\\PI_INTERNAL_REMESSA_MADEIRA.sql" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList1);

        javax.swing.GroupLayout painelPacoteAplicadoLayout = new javax.swing.GroupLayout(painelPacoteAplicado);
        painelPacoteAplicado.setLayout(painelPacoteAplicadoLayout);
        painelPacoteAplicadoLayout.setHorizontalGroup(
            painelPacoteAplicadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPacoteAplicadoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(painelPacoteAplicadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(painelPacoteAplicadoLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelPacoteAplicadoLayout.setVerticalGroup(
            painelPacoteAplicadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPacoteAplicadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelPacoteAplicadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelPacoteAplicadoLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(painelAutenticacaoBanco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(labelSelecionePacote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(labelAutenticacaoBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(labelPacoteAplicado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(painelSelecionePacote, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(painelPacoteAplicado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelAutenticacaoBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelSelecionePacote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPacoteAplicado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelSelecionePacote, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(painelAutenticacaoBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(painelPacoteAplicado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelAutenticacaoBanco.getAccessibleContext().setAccessibleName("Selecione o pacote > ");

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProsseguirAutenticacaoBdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProsseguirAutenticacaoBdActionPerformed
        irAbaAutenticacaoBanco();
    }//GEN-LAST:event_btnProsseguirAutenticacaoBdActionPerformed

    private void btnUploadFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadFileActionPerformed
        
        // ABRIR A JANELA PARA UPLOAD DE PACOTE
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
	jfc.setDialogTitle("Selecione um pacote");
        jfc.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("arquivos Compactados zip ou rar", "zip", "rar", "ZIP", "RAR");
        jfc.addChoosableFileFilter(filter);        
        
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION){
                File selectedFile = jfc.getSelectedFile();
                String newFileZip = System.getProperty("java.io.tmpdir")+selectedFile.getName();
                String pathZip = newFileZip.replaceAll(".zip", "").replaceAll(".rar", "")+File.separator;
                this.versaoPacote = StringUtil.regex(selectedFile.getName(), "[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}");
                
                try {
                    // PEGAR O CAMINHO DO ZIP E COLOCAR EM UM TEMP PARA EXTRAÇÃO E MANIPULAÇÃO
                    FileManager.copiarArquivo(selectedFile.getAbsolutePath(), newFileZip);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Erro ao copiar arquivo!\n"+ex);
                }
                
                Zip.unzip(newFileZip, pathZip);
                new File(newFileZip).delete();
                
                // VALIDA SE O PACOTE É OU NÃO VALIDO
                if(validaPathIsPackInformata(pathZip)){
                    this.diretorioPacote = pathZip;
                    btnUploadFile.setText(selectedFile.getName());
                    btnUploadFile.setIcon(null);
                    btnProsseguirAutenticacaoBd.setEnabled(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Pacote invalido!");
                    btnUploadFile.setText("");
                    btnUploadFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimidia/upload.png")));
                    btnProsseguirAutenticacaoBd.setEnabled(false);                    
                }
        }
 
    }//GEN-LAST:event_btnUploadFileActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        irAbaSelecionePacote();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnAplicarPacoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAplicarPacoteActionPerformed
        btnAplicarPacote.setText("Aplicando pacote...");
        btnAplicarPacote.setEnabled(false);
        executarScriptsPackInformata("");
        btnAplicarPacote.setText("Aplicar pacote");        
        btnAplicarPacote.setEnabled(true);
        irAbaPacoteCriado();
    }//GEN-LAST:event_btnAplicarPacoteActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        irAbaSelecionePacote();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void comboBoxConexoesTnsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxConexoesTnsActionPerformed
        habilitaBotaoAplicaPack();
    }//GEN-LAST:event_comboBoxConexoesTnsActionPerformed

    private void btnUsuarioBancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsuarioBancoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnUsuarioBancoActionPerformed

    private void btnUsuarioBancoTextValueChanged(java.awt.event.TextEvent evt) {//GEN-FIRST:event_btnUsuarioBancoTextValueChanged
        habilitaBotaoAplicaPack();
    }//GEN-LAST:event_btnUsuarioBancoTextValueChanged

    private void btnSenhaBancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSenhaBancoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSenhaBancoActionPerformed

    private void btnSenhaBancoTextValueChanged(java.awt.event.TextEvent evt) {//GEN-FIRST:event_btnSenhaBancoTextValueChanged

        // colocando mascara de password
        char arrayStr[] = btnSenhaBanco.getText().toCharArray();
        for(char c : arrayStr)
            btnSenhaBanco.setEchoChar('*');
        
        habilitaBotaoAplicaPack();
    }//GEN-LAST:event_btnSenhaBancoTextValueChanged

    private void comboBoxConexoesTnsInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_comboBoxConexoesTnsInputMethodTextChanged
        
    }//GEN-LAST:event_comboBoxConexoesTnsInputMethodTextChanged

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        executarScriptsPackInformata("Reversao");
    }//GEN-LAST:event_jButton4ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAplicarPacote;
    private javax.swing.JButton btnProsseguirAutenticacaoBd;
    private java.awt.TextField btnSenhaBanco;
    private javax.swing.JButton btnUploadFile;
    private java.awt.TextField btnUsuarioBanco;
    private javax.swing.JComboBox<String> comboBoxConexoesTns;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList<String> jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private java.awt.Label label1;
    private java.awt.Label labelAutenticacaoBanco;
    private java.awt.Label labelPacoteAplicado;
    private java.awt.Label labelSelecionePacote;
    public javax.swing.JPanel painelAutenticacaoBanco;
    private javax.swing.JPanel painelPacoteAplicado;
    public javax.swing.JPanel painelSelecionePacote;
    // End of variables declaration//GEN-END:variables


    /*----------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------- FUNÇÕES DA TELA ----------------------------------------------------*/
    /*----------------------------------------------------------------------------------------------------------------*/
    
    Font fonTitleBold   = new Font("Verdana",Font.BOLD,14);
    Font fonTitleSimple = new Font("Verdana",Font.PLAIN,14);

    public void irAbaSelecionePacote(){
        labelSelecionePacote.setFont(fonTitleBold);
        labelAutenticacaoBanco.setFont(fonTitleSimple);
        labelPacoteAplicado.setFont(fonTitleSimple);
        painelSelecionePacote.show();
        painelAutenticacaoBanco.hide(); 
        painelPacoteAplicado.hide();
        this.setSize(600, 320);
    }

    public void irAbaAutenticacaoBanco(){
        labelSelecionePacote.setFont(fonTitleSimple);
        labelAutenticacaoBanco.setFont(fonTitleBold);
        labelPacoteAplicado.setFont(fonTitleSimple);
        painelSelecionePacote.hide();
        painelAutenticacaoBanco.show();
        painelPacoteAplicado.hide();
        this.setSize(600, 340);
    }
    
    public void irAbaPacoteCriado(){
        labelSelecionePacote.setFont(fonTitleSimple);
        labelAutenticacaoBanco.setFont(fonTitleSimple);
        labelPacoteAplicado.setFont(fonTitleBold);
        painelSelecionePacote.hide();
        painelAutenticacaoBanco.hide();
        painelPacoteAplicado.show();
        this.setSize(600, 380);    
    }
    
    public void inserindoItensConnTns(JComboBox comboBox){
        ArrayList listaConnTns;
        try {
            listaConnTns = BdConexao.listaConexoesTns();
            
            for(Object itemConnTns : listaConnTns)
                comboBox.addItem((String) itemConnTns);
            
            if(listaConnTns.size() == 0)
                JOptionPane.showMessageDialog(null, "Voce não possui conexões tns!\n "
                                                  + "verifique sua variavel de ambiente ORACLE_HOME\n"
                                                  + "ou o conteudo do tnsname.ora.");                
            
            
        } catch (IOException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Erro ao alimentar itens conexao tns : "+ex);
        }       
    }
   
    public void habilitaBotaoAplicaPack(){
        String connTns   = (String) comboBoxConexoesTns.getSelectedItem();
        String usuarioBd = btnUsuarioBanco.getText();
        String senhaBd   = btnSenhaBanco.getText();
        Boolean temConexao = BdConexao.testConnectionTns(connTns, usuarioBd, senhaBd);
        btnAplicarPacote.setEnabled(temConexao);
        System.out.println(temConexao);
    }
    
    public Boolean validaPathIsPackInformata(String path){
        Boolean retorno = true;
        
        String arrArquivosExistemPacote[] = {"scr_pre_validacao.sql",
                                             "scr_valida_versao.sql",
                                             "scr_validacao.sql",
                                             "scr_versao_pacote.sql"};
        
        // se um dos arquivos arrArquivosExistemPacote nao existir na pasta return false;
        for(String nomeArquivoDoPacote : arrArquivosExistemPacote){ 
            if(!(new File(path+nomeArquivoDoPacote).exists())){
                retorno = false;
                JOptionPane.showMessageDialog(null, "arquivo "+nomeArquivoDoPacote+" não encontrado");
            }
            if(!(new File(path+"Reversao"+File.separator+nomeArquivoDoPacote).exists())){
                retorno = false;
                JOptionPane.showMessageDialog(null, "arquivo "+nomeArquivoDoPacote+" não encontrado");
            }
        }
        
        if(!(new File(path+"scr_aplica_pacote_"+this.versaoPacote+".sql").exists())){
            retorno = false;
            JOptionPane.showMessageDialog(null, "arquivo "+"scr_aplica_pacote_"+this.versaoPacote+".sql"+" não encontrado");
        }
        
        if(!(new File(path+"Reversao"+File.separator+"scr_aplica_pacote_"+this.versaoPacote+"_R.sql").exists())){
            retorno = false;
            JOptionPane.showMessageDialog(null, "arquivo "+"Reversao"+File.separator+"scr_aplica_pacote_"+this.versaoPacote+"_R.sql"+" não encontrado");
        }
        
        return retorno;
    }
    
    public void executarScriptsPackInformata(String dir){
    
        dir = (dir.equals(""))?dir:dir+File.separator;
        
        String cmdConn  = "sqlplus "+btnUsuarioBanco.getText()+"/"+btnSenhaBanco.getText()+"@"+(String) comboBoxConexoesTns.getSelectedItem();
        String cmdExecFile01 = "@"+this.diretorioPacote+dir+"scr_valida_versao.sql";
        String cmdExecFile02 = "@"+this.diretorioPacote+dir+"scr_pre_validacao.sql";
        String cmdExecFile03 = "@"+this.diretorioPacote+dir+"scr_aplica_pacote_"+this.versaoPacote+".sql";
        String cmdExecFile04 = "@"+this.diretorioPacote+dir+"scr_versao_pacote.sql";
        String cmdExecFile05 = "@"+this.diretorioPacote+dir+"scr_validacao.sql";
             
        String cmdFinal  = cmdConn+" && ";
               cmdFinal += cmdExecFile01+" && ";
               cmdFinal += cmdExecFile02+" && ";
               cmdFinal += cmdExecFile03+" && ";
               cmdFinal += cmdExecFile04+" && ";
               cmdFinal += cmdExecFile05;
        
        System.out.println(cmdFinal);
        try {
            Process obj = Runtime.getRuntime().exec("cmd.exe "+cmdFinal);
        } catch (IOException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Erro ao executar cmd: "+ex);
        }
        
    }
    
}
