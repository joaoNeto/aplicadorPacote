package gui;

import java.awt.*;
import javax.swing.*;

public class SplashScreen extends JWindow {

    public void showSplash() {        
                
        // Configura a posição e o tamanho da janela
        int width  = 450;
        int height = 200;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width-width)/2;
        int y = (screen.height-height)/2;
        setBounds(x,y,width,height);

        // imagem
        JLabel image = new JLabel(new ImageIcon(getClass().getResource("/multimidia/logo.png")));
        
        // texto versao
        JLabel copyrt = new JLabel("versao 1.0", JLabel.RIGHT);
        copyrt.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        copyrt.setForeground(Color.white);
        
        // criando a janela
        JPanel content = (JPanel)getContentPane();
        content.setBackground(Color.decode("#61c3ea"));
        content.add(image, BorderLayout.CENTER);
        content.add(copyrt, BorderLayout.SOUTH);
        
        // Torna visível
        setVisible(true);
               
    }

}