/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joao.neto
 */
public class FileManager {
    
    public static String lerArquivo(String pathFile){
    
        
        return "";
    }

    public static void copiarArquivo(String diretorioAtual, String novoDiretorio) throws Exception{
        File source = new File(diretorioAtual);
        File destination = new File(novoDiretorio);
        
        if (destination.exists())
            destination.delete();
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;

        sourceChannel = new FileInputStream(source).getChannel();
        destinationChannel = new FileOutputStream(destination).getChannel();
        sourceChannel.transferTo(0, sourceChannel.size(),
                destinationChannel);
        if (sourceChannel != null && sourceChannel.isOpen())
            sourceChannel.close();
        if (destinationChannel != null && destinationChannel.isOpen())
            destinationChannel.close();
   }
    
}
