/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author joao.neto
 */
public class StringUtil {
    
    public static String regex(String text, String regex){
        
        // Create a Pattern object
        Pattern r = Pattern.compile(regex);

        // Now create matcher object.
        Matcher m = r.matcher(text);
        if(m.find( )){
           return m.group(0);
        }
        return "";
    }
    
}
