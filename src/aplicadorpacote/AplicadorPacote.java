package aplicadorpacote;

import gui.SplashScreen;
import gui.Principal;

public class AplicadorPacote {

    public static void main(String[] args) {
        
        //splash screen
        SplashScreen splash = new SplashScreen();
        splash.showSplash();  
        
        // programa
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
                splash.setVisible(false);
            }
        });
        
    }
        
}
