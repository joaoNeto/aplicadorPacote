/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
//Bd bd = new Bd("localhost","escola_inteligente","root","","3306","mysql");
//Connection minhaCon = new BdConexao().testarConexao(bd);
//minhaCon.close();
*/
public class BdConexao {
    
    public static String pathTnsOracle   = System.getenv().get("ORACLE_HOME")+File.separator+"NETWORK"+File.separator+"ADMIN";
    public static String pathTnsNamesOra = pathTnsOracle+File.separator+"TNSNAMES.ORA";
        
    // tem erro no metodo nomes de tns com _ o sistema simplesmente ignora, ex: minha_conexao sera impresso como : minha
    public static ArrayList listaConexoesTns() throws FileNotFoundException, IOException{
        ArrayList retorno = new ArrayList();
        
        BufferedReader br = null;
        String pattern = "^([^#()\\W ][a-zA-Z0-9.]*(?:[.][a-zA-Z]*\\s?=)?)"; //Regular Expression Pattern for TNS Alias
        Pattern r = Pattern.compile(pattern);

        String sCurrentLine; 
        br = new BufferedReader(new FileReader(pathTnsNamesOra)); 
        while ((sCurrentLine = br.readLine()) != null) 
        {
            Matcher matcher = r.matcher(sCurrentLine);
            if(matcher.find()){
                String nomeTns = matcher.group().toUpperCase();
                if(existeConexaoTns(nomeTns))
                    retorno.add(nomeTns);
            }
        }
        br.close();
        return retorno;
    }

    private static Boolean existeConexaoTns(String nomeTns){
        Boolean retorno = false;
        System.setProperty("oracle.net.tns_admin",pathTnsOracle);
        String dbURL = "jdbc:oracle:thin:@"+nomeTns;

        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Erro no classname "+ex);
        }

        try {
            DriverManager.getConnection(dbURL, "", "");
            retorno = true;
        } catch (SQLException ex) {
            System.out.println("Erro ao se conectar com o banco : "+ex);
            // se for invalido apenas o usuario e senha pode passar, pois significa q a base existe 
            if(ex.toString().indexOf("ORA-01017") != -1){
                retorno = true;
            }
        }
        
        return retorno;
    }
    
    public static Boolean testConnectionTns(String nomeTns, String usuario, String senha){
        Boolean retorno = false;
        System.setProperty("oracle.net.tns_admin",pathTnsOracle);
        String dbURL = "jdbc:oracle:thin:@"+nomeTns;

        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException ex) {

        }

        try {
            DriverManager.getConnection(dbURL, usuario, senha);
            retorno = true;
        } catch (SQLException ex) {

        }
        
        return retorno;
    }
    
    
}
